import csv
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import os
import conf

wkdir = os.path.dirname(os.path.realpath(__file__))
inputs = [os.path.join(wkdir, 'csv', item) for item in
          list(filter(lambda x: x.endswith('.csv'), os.listdir(os.path.join(wkdir, 'csv'))))]


def returnaddress(fig):
    yind = 0
    for line in conf.ra:
        plt.figtext(
            figure=fig,
            x=conf.rax0,
            y=conf.ray0 - conf.raystep * yind,
            s=line,
            color='black',
            family='serif',
            size=11
        )
        yind += 1


def printaddress(fig, row):
    yind = 0

    for f in conf.fields:
        text = False
        if isinstance(f, list):
            if f[-2] == 'State' and f[-1] == 'ZIP':
                text = ', '.join([row[item] for item in f[0:-1]])
                text += '  ' + row[f[-1]]
            else:
                text = ', '.join([row[item] for item in f])

        elif f in row and row[f]:
            text = row[f]

        if text:
            plt.figtext(
                        figure=fig,
                        x=conf.adx0,
                        y=conf.ady0 - conf.adystep * yind,
                        s=text,
                        color='black',
                        family='serif',
                        size=14
                        )
            yind += 1


def main():
    with PdfPages('envelopes.pdf') as pdffile:
        fig = plt.figure(figsize=(7, 5), frameon=False)

        for file in inputs:
            with open(file, 'r') as infile:
                reader = csv.DictReader(infile)
                for row in reader:
                    if row['proofed'] == 'y' or row['proofed'] == 'x' \
                            and not row['INV ENV printed'] \
                            and row['Name'] and row['AddressLine1'] and row['City'] and row['State'] and row['ZIP']:

                        fig.clf()
                        returnaddress(fig)
                        printaddress(fig, row)
                        pdffile.savefig(fig)

    return


if __name__ == '__main__':
    main()
    exit(0)
