ra = [
    'Ashley King and Barry Muldrey',
    '689 Berne St SE, Apt C',
    'Atlanta, GA  30312'
]

fields = [
    'Name',
    'AddressLine1',
    'AddressLine2',
    'AddressLine3',
    ['City', 'State', 'ZIP']
]

adx0 = 0.325
ady0 = 0.475

rax0 = 0.05
ray0 = 0.9

adystep = 0.06
raystep = 0.04
